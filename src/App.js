import './App.css';
import React from 'react';
import Layout from './components/Layout';
import UploadFile from './components/UploadFile';
import CustomDropdown from './components/CustomDropdown';


/**
 * readCSVData is passed as a function from App.js to UploadFile.js.
 * In response to the readCSVData function, headers and data from CSV is passed back
 * @returns 
 */
function App() {
  const [csvData, setCSVData] = React.useState([]);
  const [headersText, setHeadersText] = React.useState([]);

  const readCSVData = (headerNames, data) => {
    setHeadersText(headerNames);
    setCSVData(data);
  }

  const coordinatesArr = [{'X':'1958', 'Y':'1959', 'chartType':'Scatterplot'},
  {'X':'1958', 'Y':'1959', 'chartType':'PiePlot'},
  {'X':'1958', 'Y':'1959', 'chartType':'BarChart'},
   {'X':'1959', 'Y':'1960', 'chartType':'DonutPlot'},
  {'X':'1958', 'Y':'1959', 'chartType':'BalloonPlot'},
  {'X':'1958', 'Y':'1959', 'chartType':'Line'},
  {'X':'1958', 'Y':'1959', 'chartType':'Area'},
   {'X':'1959', 'Y':'1960', 'chartType':'AsterPlot'},
    {'X':'1958', 'Y':'1960', 'chartType':'BoxWhiskerPlot'},
  {'X':'1958', 'Y':'1959', 'chartType':'Column'}, 
  {'X':'1959', 'Y':'1960', 'chartType':'ParallelPlot'},
   {'X':'1958', 'Y':'1960', 'chartType':'BubblePlot'}
];
  
  return (
    <div>
        <div className='pane-left'>
          <UploadFile readCSVData={readCSVData}></UploadFile>
          <CustomDropdown headers = {headersText}></CustomDropdown>
        </div>
        { csvData.length > 0 &&
          <div className='pane-right'>
            <Layout coordinatesArr={coordinatesArr} csvData={csvData}></Layout>
          </div>
        }
    </div>
  );
}

export default App;