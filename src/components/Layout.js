import React, { useEffect } from "react"
import * as d3 from "d3";
import Chart from "./charts/Chart"

const pad = 15
const offsetTop = 0

/**
 * The following code is converted from class based Layout.js to functional component
 * @param {csvData}
 * @returns 
 */
function Layout(props) {
    const [charts, setCharts] = React.useState([]);
    const [chartBox, setChartBox] = React.useState([], { width: window.innerWidth - 2*pad, height: window.innerHeight - offsetTop - 2*pad, top: offsetTop + pad, left: pad});
    
    const onResize = () => { 
        setChartBox({width: window.innerWidth - 2*pad, height: window.innerHeight - offsetTop - 2*pad, top: offsetTop + pad, left: pad})
    };

    useEffect(() =>  {
        var charts = []
        {props.coordinatesArr.map((value, index) => {
            generateCharts(props.csvData, value.X, value.Y, value.chartType, index, charts);
        })}
        setChartBox({ width: window.innerWidth - 2*pad, height: window.innerHeight - offsetTop - 2*pad, top: offsetTop + pad, left: pad});
        window.addEventListener('resize', onResize.bind(this), false);
    }, []);


    const generateCharts = (numData, X, Y, chartType, index, charts) => {
        var catcatData = {"data":{"Europe":{"3":0,"4":50,"5":3,"6":4,"8":0},"Japan":{"3":4,"4":51,"5":0,"6":5,"8":0},"US":{"3":0,"4":57,"5":0,"6":51,"8":78}},"max":78}
        var boxAndWhisker = [{"label":"Europe","min":7,"max":22,"median":15.8,"q1":14.5,"q3":19.5},{"label":"Japan","min":10.200000000000003,"max":19.349999999999998,"median":16.45,"q1":14.775,"q3":17.825},{"label":"US","min":7.499999999999998,"max":18.9,"median":15,"q1":13.2,"q3":17}];
        var timeData = [{"Date":'12/11/1995',"Temp":100},{"Date":'12/12/1995',"Temp":200},{"Date":'12/13/1995',"Temp":150},{"Date":'12/14/1995',"Temp":220},{"Date":'12/15/1995',"Temp":120},{"Date":'12/16/1995',"Temp":200},{"Date":'12/17/1995',"Temp":160},{"Date":'12/18/1995',"Temp":300},{"Date":'12/19/1995',"Temp":130}]
        var aster = [{category:"Team A",points:8},{category:"Team B",points:5},{category:"Team C",points:7},{category:"Team D",points:9},{category:"Team E",points:4},{category:"Team F",points:14},{category:"Team G",points:6},{category:"Team H",points:12},{category:"Team I",points:2},{category:"Team J",points:20},{category:"Team K",points:18}]
        var parallel_data = [{"1958":340,"1959":360,"1960":417,"Month":"January"},{"1958":318,"1959":342,"1960":391,"Month":"February"},{"1958":362,"1959":406,"1960":419,"Month":"March"},{"1958":348,"1959":396,"1960":461,"Month":"April"},{"1958":363,"1959":420,"1960":472,"Month":"May"},{"1958":435,"1959":472,"1960":535,"Month":"June"},{"1958":491,"1959":548,"1960":622,"Month":"July"},{"1958":505,"1959":559,"1960":606,"Month":"August"},{"1958":404,"1959":463,"1960":508,"Month":"September"},{"1958":359,"1959":407,"1960":461,"Month":"October"},{"1958":310,"1959":362,"1960":390,"Month":"November"},{"1958":337,"1959":405,"1960":432,"Month":"December"}]
        var bubble_data = [{"1958":340,"1959":360,"1960":417,"Month":"1"},{"1958":318,"1959":342,"1960":391,"Month":"2"},{"1958":362,"1959":406,"1960":419,"Month":"3"},{"1958":348,"1959":396,"1960":461,"Month":"4"},{"1958":363,"1959":420,"1960":472,"Month":"2"},{"1958":435,"1959":472,"1960":535,"Month":"6"},{"1958":491,"1959":548,"1960":622,"Month":"2"},{"1958":505,"1959":559,"1960":606,"Month":"3"},{"1958":404,"1959":463,"1960":508,"Month":"1"},{"1958":359,"1959":407,"1960":461,"Month":"6"},{"1958":310,"1959":362,"1960":390,"Month":"4"},{"1958":337,"1959":405,"1960":432,"Month":"3"}]
        var chartWidth = 320
        var chartHeight = 250
        var chartPadding = 55
        var chartConfigList = [
            {
                label: "Scatterplot",   //should come from demoArr
                chart: <Chart type={"ScatterPlot"} data={numData} X={X} Y={Y} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "BarChart",
                chart: <Chart type={"BarChart"} data={numData} X={X} Y={Y} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "BalloonPlot",
                chart: <Chart type={"BalloonPlot"} data={catcatData} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "HeatMap",
                chart: <Chart type={"HeatMap"} data={catcatData} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "Line",
                chart: <Chart type={"Line"} data={timeData} X={"Date"} Y={"Temp"} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "Area",
                chart: <Chart type={"Area"} data={timeData} X={"Date"} Y={"Temp"} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "Column",
                chart: <Chart type={"Column"} data={numData} X={Y} Y={X} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "StripPlot",
                chart: <Chart type={"StripPlot"} data={numData} X={Y} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "AsterPlot",
                chart: <Chart type={"AsterPlot"} data={aster} X={"category"} Y={"points"} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "BoxWhiskerPlot",
                chart: <Chart type={"BoxWhiskerPlot"} data={boxAndWhisker} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "PiePlot",
                chart: <Chart type={"PiePlot"} data={aster} X={"category"} Y={"points"} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "DonutPlot",
                chart: <Chart type={"DonutPlot"} data={aster} X={"category"} Y={"points"} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "ParallelPlot",
                chart: <Chart type={"ParallelPlot"} data={parallel_data} attributes={[{name: "1958", type:"Numerical"}, {name: "1959", type:"Numerical"}, {name: "1960", type:"Numerical"},{name: "Month", type:"Nominal"} ]} width={2*chartWidth} height={chartHeight} padding={chartPadding} />
            },
            {
                label: "BubblePlot",
                chart: <Chart type={"BubblePlot"} data={bubble_data} X={"1958"} Y={"1959"} R={"Month"} width={chartWidth} height={chartHeight} padding={chartPadding} />
            },


            


        ]

        for(var i=0; i< chartConfigList.length; i++){
            if (chartType == chartConfigList[i].label) {
                var chart =  <div id={index}  key={"chart"+i+index} style={{display: "inline-block", margin: "0px", padding:"10px"}}>      
                                <label style={{color: "#000000", display: "block"}}>{chartConfigList[i].label}</label>                        
                                {chartConfigList[i].chart}                     
                            </div>
                charts.push(chart)
            }
        }
    
        setCharts(charts);
    }

    return (
        <div className="contentdiv">
            <label className="contendDivHead" >Charts</label>
            <div className="contentInnerdiv">
                {charts}
            </div>
        </div>
    );
}

export default Layout;