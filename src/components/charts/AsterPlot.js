import React from 'react';
import * as d3 from "d3";

const radScale = (thickness, data, value) => {
  return d3.scaleLinear()
    .domain([0, d3.max(data, function(d) { return +d[value] })])
    .range([0, thickness]);
};

var colorScale = d3.scaleOrdinal(d3.schemeCategory10);

const renderArcs = (rad, thickness, radScale, data, label, value) => {
  return (d,index) => {
    const arc = d3.arc()
                  .innerRadius(rad - thickness)
                  .outerRadius(rad - thickness + radScale(data[index][value]));
    
    var arcColor = colorScale(d.data[label]);
    var pathProps = {
      key: index,
      d: arc(d),
      fill: arcColor
    }
    return <path {...pathProps}><title>{data[index][value]}</title></path>
  }
}

const renderLabels = (label, rad) => {
    return (d,index) => {
      const arc = d3.arc()
                    .innerRadius(rad )
                    .outerRadius(rad+20);
      if(d.startAngle-d.endAngle !==0 ) {
        return <text key={index} transform={`translate(${arc.centroid(d)})`} textAnchor={"middle"} fontSize={10}>{d.data[label]}</text>
      }
      else {
        return null;
      }
    }
  }


  function AsterPlot(props) {
    if(props.data.length < 1) {
        return null
      }
      
      else {

        var thickness = 50
        var rSc = radScale(thickness, props.data, props.Y);
        let pie = d3.pie()
                    .sort(null)
                    .value((d) => d[props.Y])(props.data);
        
        return (
          <svg width={props.width} height={props.height}>
            <g transform={`translate(${props.width/2}, ${props.height/2 + 10})`} >
              {pie.map(renderArcs(props.height/2 -props.padding, thickness, rSc, props.data, props.X, props.Y))}
              {pie.map(renderLabels(props.X, (props.height/2 -props.padding)))}
            </g>
          </svg>
        );
      }

  }

export default AsterPlot;