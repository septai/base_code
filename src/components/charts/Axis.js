import React from 'react';
import * as d3 from "d3";

export default class Axis extends React.Component {
  componentDidMount() {
    this.renderAxis();
  }

  componentDidUpdate() {
    this.renderAxis();
  }

  renderAxis() {
    var node  = this.refs.axis;
    switch(this.props.orient) {
      case "left": {
        var axis = d3.axisLeft(this.props.scale);
        axis.ticks(this.props.ticks);
        d3.select(node).call(axis);
        break;
      }
      case "right": {
        var axis = d3.axisRight(this.props.scale);
        axis.ticks(this.props.ticks);
        d3.select(node).call(axis);
        break;
      }
      case "top": {
        var axis = d3.axisTop(this.props.scale);
        axis.ticks(this.props.ticks);

        d3.select(node)
          .call(axis)
          .selectAll("text")
          .attr("y", 0)
          .attr("x", 0)
          .attr('dy', -10)                    
        break;
      }
      case "bottom": {
        var axis = d3.axisBottom(this.props.scale);
        axis.ticks(this.props.ticks);

        d3.select(node)
          .call(axis)
          .selectAll("text")
          .attr("y", 0)
          .attr("x", -5)
          .attr('dy', 12)
          .attr("transform", "rotate(-45)")
          .style("text-anchor", "end")          
        break;
      }
    }
    
  }

  render() {
    return <g>
              <g className="axis" ref="axis" transform={this.props.transform}></g>      
              <text x={this.props.txtX} y={this.props.txtY} fill="black" fontSize="12"  strokeWidth="0.5" stroke="#000000" textAnchor={this.props.textAnchor}>{this.props.name}</text>
           </g>
  }
}