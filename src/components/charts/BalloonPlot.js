import React from 'react';
import * as d3 from "d3";

const rScale = (max, rad) => {   
    return d3.scaleLinear()
      .domain([0, max])
      .range([0, rad]);
  };
  
  
  const renderGridCell = (data, yLoc, vertPad, horzPad, cellSide, row, rSc) => {
    return (d, index) => {
      const rectProps = {
        x: horzPad + index*cellSide,
        y: yLoc,
        width: cellSide,
        height: cellSide,
        fill: "none",
        stroke: "#000000",
      };
      
      const circleProps = {
        r: rSc(data[d])/2,
        cx: horzPad + index*cellSide + cellSide/2,
        cy: yLoc + cellSide/2,
        strokeWidth: 1,
        opacity:1,
        fill: "#6fa3ff",
        className: 'scatter',
        key: index,
      };
      
      if(row == 0) {
        var transform = "translate("+ (horzPad+cellSide*(index)+(cellSide/2)) + "," + (+yLoc - 2) +") rotate(-45)"
        return <g key={index} >
                <text fill="black" fontSize="11" transform={transform}>{d}</text>
                <rect {...rectProps} />
                <circle {...circleProps}><title>{"N: " + data[d]}</title></circle>
               </g>
      }
  
      
      return <g key={index} >    
                <rect {...rectProps} />
                <circle {...circleProps}><title>{"N: " + data[d]}</title></circle>;
              </g>
    };
  };

  const renderGridRow = (data, yLabels, vertPad, horzPad, cellSide, rSc) => {
    return (d, index) => {
      var yLoc = vertPad + index*cellSide;    
  
      return <g key={index}>
              <text x={horzPad - 4} y={yLoc+14} fill="black" fontSize="11" textAnchor="end">{d}</text>
              {yLabels.map(renderGridCell(data[d], yLoc, vertPad, horzPad, cellSide, index, rSc))}
             </g>;
    };
  };

  function BalloonPlot(props) {
    var y = Object.keys(props.data.data)
    var x = Object.keys(props.data.data[y[0]])    

   
    var sidex = (props.width - 2*props.padding) / x.length ;
    var sidey = (props.height - 2*props.padding) / y.length ;
    var vertPad = props.padding;
    var horzPad = props.padding;
    var cellSide = sidex < sidey ? sidex : sidey;
    var rSc = rScale(props.data.max, cellSide-4)

    return (
        <svg width={props.width} height={props.height}>
           {y.map(renderGridRow(props.data.data, x, vertPad, horzPad, cellSide, rSc))}          
        </svg>
   
 );

  }

export default BalloonPlot;