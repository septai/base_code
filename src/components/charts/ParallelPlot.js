import React from 'react';
import * as d3 from "d3";
import Axis   from './Axis';

const yScale = (height, padding, data, Y) => {
  return d3.scaleLinear()
    .domain([d3.min(data, function(d) { return +d[Y] }), d3.max(data, function(d) { return +d[Y] })])
    .range([height - padding, padding]);
};

const yScaleCat = (height, padding, data, Y) => {
  return d3.scaleBand()
    .domain(data.map(function(d) { return d[Y]; }))
    .range([height - padding, padding]);
};

const renderAxes = (padding) => {
  return (axis, index) => {
    const ySettings = {
      transform: `translate(${+axis.x}, 0)`,
      scale: axis.scale,
      orient: 'left',
      key: "Axis-"+index,
    };
    var labelX = axis.x - padding/2   
    return <Axis {...ySettings} name={axis.label} txtX={labelX} txtY={padding-10}/>
  }
};

const renderLines = (axes, C) => {
  return (d, index) => {
    var data = [];

    axes.forEach( function(axis,i) {
      var tmp ={x: axis.x, y: axis.scale(d[axis.label])} 
      if( axis.type != "Numerical"){
        tmp.y = axis.scale(d[axis.label]) + axis.scale.bandwidth()/2
      }
      data.push(tmp);
    });

    var line = d3.line()
                 .x(function(d) { return d.x; })
                 .y(function(d) { return d.y; });

    var pathProps = {
                      d: line(data),
                      stroke: "#800080",
                      strokeWidth:1,
                      opacity: 0.2,
                      fill:"none"
                    };

    return <path key={index} {...pathProps}> </path>;
  }
};

function ParallelPlot(props){
    if(props.data.length < 1) {
        return null
      }
      
      var axDist = (props.width-2*props.padding) / (props.attributes.length-1);
      var axes = []
      for(var i=0; i < props.attributes.length; i++) {
        var yAx = null
       
        if(props.attributes[i].type == "Numerical"){
          yAx = yScale(props.height, props.padding, props.data, props.attributes[i].name); //Axis scale
        }
        else{        
          yAx = yScaleCat(props.height, props.padding, props.data, props.attributes[i].name); //Axis scale
        }
        
        axes.push({
          scale: yAx,
          label: props.attributes[i].name,
          type: props.attributes[i].type,
          x: props.padding - 5 + i*axDist,
        });
      }
  
      return (
          <svg width={props.width} height={props.height}>
            {props.data.map(renderLines(axes))}
            {axes.map(renderAxes(props.padding))}
          </svg>
      );
}
export default ParallelPlot;