import React from 'react';
import * as d3 from "d3";


var colorScale = d3.scaleOrdinal(d3.schemeCategory10);

const renderArcs = (rad, thickness, data, label, value) => {
  return (d,index) => {
    const arc = d3.arc()
                  .innerRadius(rad-thickness)
                  .outerRadius(rad);

    var arcColor = colorScale(d.data[label]);
    var pathProps = {
      key: index,
      d: arc(d),
      fill: arcColor
    }
    return <path {...pathProps}><title>{data[index][value]}</title></path>
  }
}

const renderLabels = (label, rad) => {
  return (d,index) => {
    const arc = d3.arc()
                  .innerRadius(rad )
                  .outerRadius(rad+20);
    if(d.startAngle-d.endAngle !==0 ) {
      return <text key={index} transform={`translate(${arc.centroid(d)})`} textAnchor={"middle"} fontSize={10}>{d.data[label]}</text>
    }
    else {
      return null;
    }
  }
}

function DonutPlot(props){
    if(props.data.length < 1) {
        return null
      }
      else {
        let pie = d3.pie()
                    .sort(null)
                    .value((d) => d[props.Y])(props.data);
        return (
          <svg width={props.width} height={props.height}>
            <g transform={`translate(${props.width/2}, ${props.height/2 + 10})`} >
              {pie.map(renderArcs(props.height/2 -props.padding, 30, props.data, props.X, props.Y))}
              {pie.map(renderLabels(props.X, (props.height/2 -props.padding)))}
            </g>
          </svg>
        );
      }
}
export default DonutPlot;