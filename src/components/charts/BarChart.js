import * as d3 from "d3";

import XYAxis from './AxisXY';

    const xScale = (width, padding, data, X) => {

        return  d3.scaleBand()

            .domain(data.map((d) => d[X]))

            .range([padding,width])

            .padding(0.2);

      };

      

      const yScale = (height, padding, data, Y) => {

        return d3.scaleLinear()

            .domain([0,d3.max(data, function(d) { return +d[Y] })])

            .range([height-padding,padding]);

      };

      

    

      const renderRects = (padding, height, xAx, yAx, X, Y) => {

       

        return (d, index) => {

          const rectProps = {

            x:  xAx(d[X]),

            y: yAx(d[Y]),

            width: xAx.bandwidth(),

            height: (height - padding - yAx(d[Y])),

            fill: "#6fa3ff",

            key: index,

          }


          return <rect {...rectProps} ><title>{d[Y]}</title></rect>;

        };

      };

    

      function BarChart(props) {

        // render() {

          if(props.data.length < 1) {

            return null

          }

          const xAx = xScale(props.width, props.padding, props.data, props.X); //Axis scale    

          const yAx = yScale(props.height, props.padding, props.data, props.Y); //Axis scale
      

          

          return (

              <svg width={props.width} height={props.height}>

                {props.data.map(renderRects(props.padding,props.height,xAx, yAx, props.X, props.Y)) }

                <XYAxis {...props} xScale={xAx} yScale={yAx} X={props.X} Y={props.Y}/>

              </svg>

          );

        // }

      }

      

export default BarChart;