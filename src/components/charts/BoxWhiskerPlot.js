import React from 'react';
import * as d3 from "d3";
import XYAxis from './AxisXY';

const xScale = (width, height, padding, data, X) => {
    return d3.scaleBand()
      .domain(data.map(function(d) { return d[X]; }))
      .range([padding, width - padding]);
  };
  
  const yScale = (width, height, padding, data) => {
    return d3.scaleLinear()
      .domain([d3.min(data, function(d) { return d['min'] }), d3.max(data, function(d) { return d['max'] })])    
      .range([height - padding, padding]);
  };

  const renderBoxes = (padding, xAx, yAx) => {
    return (d, index) => {    
      const rectProps = {
        x:  xAx(d['label']) + xAx.bandwidth()/4,
        y: yAx(d.q3),
        width: xAx.bandwidth()/2,
        height: yAx(d.q1) - yAx(d.q3),
        fill: "#80c4ff",
        stroke: "#002e56" ,
        strokeWidth: 1,
      }
  
      const lineProps = {
        x1: xAx(d['label']) + xAx.bandwidth()/4,
        y1: yAx(d.median),
        x2:  xAx(d['label']) + xAx.bandwidth()/4 + xAx.bandwidth()/2,
        y2: yAx(d.median),
        fill: "none",
        stroke: "#002e56" ,
        strokeWidth: 1,
      }
  
      const lineExtProps = {
        x1: xAx(d['label']) + xAx.bandwidth()/2,
        y1: yAx(d.min),
        x2: xAx(d['label']) + xAx.bandwidth()/2,
        y2: yAx(d.max),
        fill: "none",
        stroke: "#002e56" ,
        strokeWidth: 1,
      }
    
      const lineMinProps = {
        x1:  xAx(d['label']) + xAx.bandwidth()/4,
        y1: yAx(d.min),
        x2: xAx(d['label']) + xAx.bandwidth()/4 + xAx.bandwidth()/2,
        y2: yAx(d.min),
        fill: "none",
        stroke: "#002e56" ,
        strokeWidth: 1,
      }
    
      const lineMaxProps = {
        x1: xAx(d['label']) + xAx.bandwidth()/4,
        y1: yAx(d.max),
        x2: xAx(d['label']) + xAx.bandwidth()/4 + xAx.bandwidth()/2,
        y2: yAx(d.max),
        fill: "none",
        stroke: "#002e56" ,
        strokeWidth: 1,
      }
      return <g key={"box"+index}>
                <line {...lineExtProps} ></line>
                <line {...lineMinProps} ></line>
                <line {...lineMaxProps} ></line>
                <rect {...rectProps} ></rect>
                <line {...lineProps} ></line>
            </g>;
    }
  }
  
  function BoxWhiskerPlot(props) {
    
    if(props.data == null) {
        return null
      }        
      
      var xAx= xScale(props.width, props.height, props.padding, props.data, "label"); //Axis scale
      var yAx= yScale(props.width, props.height, props.padding, props.data); //Axis scale
  
      return (
          <svg width={props.width} height={props.height}>
            {props.data.map(renderBoxes(props.padding, xAx, yAx)) }
            <XYAxis {...props} xScale={xAx} yScale={yAx} X={props.X} Y={props.Y}/>
          </svg>
      );

  }

export default BoxWhiskerPlot;