import React from 'react';
import * as d3 from "d3";
import XYAxis from './AxisXY';

const xScale = (width,  padding, data, X) => {
    return d3.scaleTime()
      .domain([new Date(data[0][X]), new Date(data[data.length-1][X])])
      .range([padding, width - padding]);
  };
 
  
  const yScale = (height, padding, data, Y) => {
    return d3.scaleLinear()
      .domain([d3.min(data, function(d) { return +d[Y] })-20, d3.max(data, function(d) { return +d[Y] })])
      .range([height - padding, padding]);
  };

  const renderLine = (data, X, Y, xAx, yAx) => {

    var line = d3.line()
                  .x(function(d) { return xAx(new Date(d[X])) })
                  .y(function(d) { return yAx(+d[Y]); });

    var pathProps = {
      d: line(data),
      stroke:"#6fa3ff",
      strokeWidth:2,
      fill:"none"
    }
    return <path {...pathProps} ></path>;
};
function Line(props) {
    if(props.data.length < 1) {
        return null
      }

      var xAx= xScale(props.width, props.padding, props.data, props.X); //X-Axis scale
      var yAx= yScale(props.height, props.padding, props.data, props.Y); //Y-Axis scale
      return (
          <svg width={props.width} height={props.height}>
            {renderLine(props.data, props.X, props.Y, xAx, yAx)}
            <XYAxis {...props} xScale={xAx} yScale={yAx} X={props.X} Y={props.Y}/>
          </svg>
      );

  }

export default Line;