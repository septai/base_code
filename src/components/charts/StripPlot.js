import React from 'react';
import * as d3 from "d3";
import Axis   from './Axis';

const xScale = (width, height, padding, data, X) => {
    return d3.scaleLinear()
      .domain([d3.min(data, function(d) { return +d[X] }), d3.max(data, function(d) { return +d[X] })])
      .range([padding, width - padding]);
  };

  const renderPoints = (xAx, X, Y, ht) => {
    return (d, index) => {
      const circleProps = {
        x1: xAx(d[X]),
        y1: Y,
        x2: xAx(d[X]),
        y2: Y-ht,
        stroke: "#6fa3ff" ,
        strokeWidth: 1,
        opacity: 0.5,      
        key: index,
      }
      return <line {...circleProps}/>;
    };
  };

  function StripPlot(props) {
    if(props.data.length < 1) {
        return null
      }
      var xAx= xScale(props.width, props.height, props.padding, props.data, props.X); //Axis scale
  
      const xSettings = {
        transform: `translate(0, ${props.height - props.padding})`,
        scale: xAx,
        orient: 'bottom',
        textAnchor: 'end'
      };
      
      var Y = props.height - props.padding - 2
      
      return (
          <svg width={props.width} height={props.height}>
            {props.data.map(renderPoints(xAx, props.X, Y, 20)) }
            <Axis {...xSettings} name={props.X} txtX={props.width-props.padding}  txtY={props.height - 15} ticks={5}/>
          </svg>
      );

  }

export default StripPlot;