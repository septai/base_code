import React from 'react';
import * as d3 from "d3";
import Axis   from './Axis';

const xScale = (width, padding, data, Y) => {
  return d3.scaleLinear()
    .domain([0, d3.max(data, function(d) { return +d[Y] })])
    .range([padding, width - padding]);
};

const yScale = (height, padding, data, X) => {
  return d3.scaleBand()
    .domain(data.map(function(d) { return d[X]; }))
    .range([padding, height - padding ]);
};

const renderRects = (padding, height, xAx, yAx, X, Y) => {
  return (d, index) => {
    const rectProps = {
      x:  padding ,
      y: yAx(d[Y]) + yAx.bandwidth()/4,
      width: xAx(d[X]) - padding,
      height: yAx.bandwidth()/2,
      fill: "#6fa3ff",
      key: index,
    }
    return <rect {...rectProps} ><title>{d[X]}</title></rect>;
  };
};



function Column(props) {
    // render() {
        if(props.data.length < 1) {
            return null
          }
          
          var xAx= xScale(props.width, props.padding, props.data, props.X); //Axis scale
          var yAx= yScale(props.height, props.padding, props.data, props.Y); //Axis scale
      
          const xSettings = {
            transform: `translate(0, ${props.padding})`,
            scale: xAx,
            orient: 'top',
            textAnchor: 'end'
          };
          const ySettings = {
            transform: `translate(${props.padding}, 0)`,
            scale: yAx,
            orient: 'left',
            textAnchor: 'end'
          };
      
          return (
              <svg width={props.width} height={props.height}>
                {props.data.map(renderRects(props.padding, props.height, xAx, yAx, props.X, props.Y, props.C))}          
                <Axis {...xSettings} name={props.X} txtX={props.width-props.padding}  txtY={props.padding - 20} ticks={5}/>
                <Axis {...ySettings} name={props.Y} txtX={props.padding} txtY={props.height - props.padding + 15} ticks={5}/>
              </svg>
          );
  }
export default Column;