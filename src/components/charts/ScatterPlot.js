import React from 'react';
import * as d3 from "d3";
import XYAxis from './AxisXY';

const xScale = (width, padding, data, X) => {
  return d3.scaleLinear()
    .domain([d3.min(data, function(d) { return +d[X] }), d3.max(data, function(d) { return +d[X] })])
    .range([padding, width - padding]);
};

const yScale = (height, padding, data, Y) => {
  return d3.scaleLinear()
    .domain([d3.min(data, function(d) { return +d[Y] }), d3.max(data, function(d) { return +d[Y] })])
    .range([height - padding, padding]);
};

const renderPoints = (xAx, yAx, X, Y) => {
  return (d, index) => {
    const circleProps = {
      cx: xAx(d[X]),
      cy: yAx(d[Y]),
      stroke: "#ffffff",
      strokeWidth: 1,
      opacity: 0.75,
      r: 3,
      fill: "#6fa3ff",
      key: index,
    }
    return <circle {...circleProps}><title>{"a:b"}</title></circle>;
  };
};

// export default class ScatterPlot extends React.Component {
function ScatterPlot(props) {
    if(props.data.length < 1) {
      return null
    }
    const xAx = xScale(props.width, props.padding, props.data, props.X); //Axis scale    
    const yAx = yScale(props.height, props.padding, props.data, props.Y); //Axis scale

    
    return (
        <svg width={props.width} height={props.height}>
          {props.data.map(renderPoints(xAx, yAx, props.X, props.Y)) }
          <XYAxis {...props} xScale={xAx} yScale={yAx} X={props.X} Y={props.Y}/>
        </svg>
    );
}

export default ScatterPlot;