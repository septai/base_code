import React  from 'react';
import Axis   from './Axis';

function XYAxis(props) {
  const xSettings = {
    transform: `translate(0, ${props.height - props.padding})`,
    scale: props.xScale,
    orient: 'bottom',
    textAnchor: 'end'
  };
  const ySettings = {
    transform: `translate(${props.padding}, 0)`,
    scale: props.yScale,
    orient: 'left',
    textAnchor: 'middle'
  };
  return(
    <g className="xy-axis">
      <Axis {...xSettings} name={props.X} txtX={props.width-props.padding}  txtY={props.height - 10} ticks={5}/>
      <Axis {...ySettings} name={props.Y} txtX={props.padding} txtY={props.padding-10} ticks={5}/>
    </g>
  );
}

export default XYAxis;