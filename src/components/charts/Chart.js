import React  from 'react';
import StripPlot from './StripPlot'
import Column from './Column'
// import Bar from './Bar'
import PiePlot from './PiePlot'
import DonutPlot from './DonutPlot'
import AsterPlot from './AsterPlot'
import BalloonPlot from './BalloonPlot'
import HeatMap from './HeatMap'
import Line from './Line'
import Area from './Area'
import BarChart from "./BarChart"
import ScatterPlot from './ScatterPlot'
import BubblePlot from './BubblePlot'
import ParallelPlot from './ParallelPlot'
import BoxWhiskerPlot from './BoxWhiskerPlot'

function Chart(props) {
    const getChart = (type, props) => {
        var charts = {        
          'StripPlot':   <StripPlot {...props}/>,
          'BoxWhiskerPlot':   <BoxWhiskerPlot {...props}/>,
          // 'Bar': <Bar {...props}/>,           
          'Column': <Column {...props}/>, 
          'PiePlot': <PiePlot {...props}/>,    
          'DonutPlot': <DonutPlot {...props}/>,    
          'AsterPlot': <AsterPlot {...props}/>,    
          'BalloonPlot':   <BalloonPlot {...props}/>, 
          'HeatMap':   <HeatMap {...props}/>,     
          'Line':   <Line {...props}/>,    
          'Area':   <Area {...props}/>,   
          'BarChart': <BarChart {...props}></BarChart>,
          'ScatterPlot': <ScatterPlot {...props}/>,
          'BubblePlot': <BubblePlot {...props}/>,
          'ParallelPlot': <ParallelPlot {...props}/>,
          'default': <svg width={props.width} height={props.height}><text x={10} y={10}>No Chart</text></svg>
        };
        return (charts[type] || charts['default']);
      }

      return(
        getChart(props.type, props)
      );
}

export default Chart;