import React from 'react';
import Select from 'react-select';



export default class CustomDropdown extends React.Component {
  state = {
    selectedOption: null,
  };
  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };
  render() {
    let options = [];
    if (this.props.headers.length > 0) {
      this.props.headers.map((row) => (options.push({value:row, label:row})))
    }
    
    const { selectedOption } = this.state;

    return (
      <Select
        isMulti
        value={selectedOption}
        onChange={this.handleChange}
        options={options}
      />
    );
  }
}