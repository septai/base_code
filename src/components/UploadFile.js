import React from 'react';
import axios from 'axios';
import * as d3 from "d3";

function UploadFile(props) {
    const submitFileToServer = (event) => {
        event.preventDefault();
        const data = new FormData();
        try {
            const res = axios({
                method: "post",
                url: "http://localhost:3000/visualization",
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            });
        } catch (err) {
            console.log(err);
        }
    };

    const readCSVFile = (event) => {
        event.preventDefault();
        d3.csv('./data/airtravel.csv', function(error, data) {
            if (error) throw error;
            if (data.length > 0) {
                console.log(data)
                const headerNames = d3.keys(data[0]);
                props.readCSVData(headerNames, data);
            }
        });
    }

    return (
        <div>
            <form onSubmit={readCSVFile}>
                <input type="file" />
                <input type="submit" value="Read CSV" />
            </form>
        </div>
    )
};

export default UploadFile;