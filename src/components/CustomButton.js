import React from 'react';
import {Button} from 'react-bootstrap';

function CustomButton(props) {
	return (
		<Button>props.text</Button>
	);
}

export default CustomButton;